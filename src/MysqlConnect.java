
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class MysqlConnect {
    Connection conn;
    
    /*
    Returns a valid Connection Object if it can establish connection with the empdb.
    Else returns null
    */
    public static Connection connectDB(){
        try{
//            protocol://hostname:port_no/path_to_resources
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/empdb", "chintan2", "chintan");
//            JOptionPane.showMessageDialog(null, "Connection Established Successfully!");
            return conn;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Connection Failed! " + e);
            return null;
        }
    }
}
