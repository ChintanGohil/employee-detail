
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import net.coobird.thumbnailator.Thumbnails;
import net.proteanit.sql.DbUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MAHESH
 */
public class EmployeeInfo extends javax.swing.JFrame {

    Connection conn = null;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    /*Java sab constant public rahkta h private nai kyuki agar bahar se insert update karna ho toh kar sakta h*/
    public static final int NOP = 0;
    public static final int INSERT = 1;
    public static final int UPDATE = 2;
    
    /**
     * Creates new form Admin
     */
    public EmployeeInfo() {
        initComponents();
        conn = MysqlConnect.connectDB();
//        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
//        this.setLocation(dimension.width / 2 - this.getWidth() / 2, dimension.height / 2 - this.getHeight() / 2);
        this.setLocationRelativeTo(null);
        String sql = "SELECT * FROM employeeinfo";
        updateTableData(sql);
        disableAll();
        txtSearch.getDocument().addDocumentListener(new DocumentListener(){
           @Override
           public void insertUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void removeUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void changedUpdate(DocumentEvent e){
           }
           
        });
    }

    private DefaultTableModel getMyTableModel (TableModel dtm){
        
        int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
        Object[][] tableData = new Object[nRow][nCol];
        for(int i=0; i<nRow; i++)
            for(int j=0; j<nCol; j++)
                tableData[i][j] = dtm.getValueAt(i, j);
        String[] colHeads = {"ID", "Name", "Gender", "Age", "Blood Group", "Contact No", "Qualification", "DOJ", "Address", "Image"};
        DefaultTableModel myModel = new DefaultTableModel(tableData, colHeads){
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        return myModel;
    }
    
    private void enableAll(){
        txtEmpName.setEnabled(true);
        rbtMale.setEnabled(true);
        rbtFemale.setEnabled(true);
        txtEmpAge.setEnabled(true);
        cmbBloodGroup.setEnabled(true);
        txtContactNo.setEnabled(true);
        cmbQualification.setEnabled(true);
        jdcDOJ.setEnabled(true);
        taAddress.setEnabled(true);
        btnUploadImage.setEnabled(true);
    }
    
    /*
        Yeh do method ki place mai you can create one method name as toggleAll() as shown below
        
        private void toggleAll(){
            txtEmpName.setEnabled(!isEnabled());
        }
    
        Matlab agar voh enable h toh isEnabled true dega aur '!' will make it false and disable kar dega
    */
    
    
    private void disableAll(){
        txtEmpName.setEnabled(false);
        rbtMale.setEnabled(false);
        rbtFemale.setEnabled(false);
        txtEmpAge.setEnabled(false);
        cmbBloodGroup.setEnabled(false);
        txtContactNo.setEnabled(false);
        cmbQualification.setEnabled(false);
        jdcDOJ.setEnabled(false);
        taAddress.setEnabled(false);
        txtImagePath.setEnabled(false); 
    }
    
    private void searchFilter(){
        String key = txtSearch.getText();
        String sql;
        if(key.equals("")){
            sql = "SELECT * FROM employeeinfo";
        }else{
            sql = "SELECT * FROM employeeinfo WHERE name like '%"+key+"%'";
        }
        updateTableData(sql);
    }
    
    private void insertRecord(){
        String sql = "INSERT INTO employeeinfo(name, gender, age, blood_group, contact_no, qualification, doj, address, employee_image) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        String name = txtEmpName.getText();
        String age = txtEmpAge.getText();
        String blood_group = cmbBloodGroup.getSelectedItem().toString();
        String contact_no = txtContactNo.getText();
        String qualification = cmbQualification.getSelectedItem().toString();
        
        Date selectedDate = jdcDOJ.getDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = sdf.format(selectedDate);
        
        String address = taAddress.getText();
        
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, gender);
            ps.setString(3, age);
            ps.setString(4, blood_group);
            ps.setString(5, contact_no);
            ps.setString(6, qualification);
            ps.setString(7, formattedDate);
            ps.setString(8, address);
            ps.setBytes(9, byteImage);
            
            ps.execute();
            clearFields();
            String loadTableQuery = "SELECT * FROM employeeinfo";
            updateTableData(loadTableQuery);
            JOptionPane.showMessageDialog(this, "Record Inserted Successfully!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Error while Inserting Record!" + ex.getMessage());
        }
    }
    
    private void updateRecord(){
        
        if(!"".equals(txtEmpID.getText())){
            String sql = "UPDATE employeeinfo SET name = ?, gender = ?, age = ?, blood_group = ?, contact_no = ?, qualification = ?, doj = ?, address = ?, employee_image = ? WHERE employee_id = ?";
            String name = txtEmpName.getText();
            String age = txtEmpAge.getText();
            String blood_group = cmbBloodGroup.getSelectedItem().toString();
            String contact_no = txtContactNo.getText();
            String qualification = cmbQualification.getSelectedItem().toString();

            Date selectedDate = jdcDOJ.getDate();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = sdf.format(selectedDate);

            String address = taAddress.getText();

            try {
                ps = conn.prepareStatement(sql);
                ps.setString(1, name);
                ps.setString(2, gender);
                ps.setString(3, age);
                ps.setString(4, blood_group);
                ps.setString(5, contact_no);
                ps.setString(6, qualification);
                ps.setString(7, formattedDate);
                ps.setString(8, address);
                ps.setBytes(9, byteImage);
                ps.setInt(10, Integer.parseInt(txtEmpID.getText()));

                ps.execute();
                clearFields();
                String loadTableQuery = "SELECT * FROM employeeinfo";
                updateTableData(loadTableQuery);
                JOptionPane.showMessageDialog(this, "Record Updated Successfully!");
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Error while Updating Record!" + ex.getMessage());
            }
       }
    }
    
    private void updateTableData(String sql){
        try{
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
//            jtbData.setModel(DbUtils.resultSetToTableModel(rs));
            jtbData.setModel(getMyTableModel(DbUtils.resultSetToTableModel(rs)));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "Error While Fetching Data from Database!");
        }finally{
            try{
                rs.close();
                ps.close();
            }catch(SQLException ex){
                JOptionPane.showMessageDialog(this, "Error While Closing ps or rs!");
            }
        }
    }
    
    private void clearFields(){
        txtEmpID.setText("");
        txtEmpName.setText("");
        
        rbtTemp.setSelected(true);
        gender = null;
        
        txtEmpAge.setText("");
        cmbBloodGroup.setSelectedIndex(-1);
        txtContactNo.setText("");
        cmbQualification.setSelectedIndex(-1);
        
        jdcDOJ.setDate(null);
        taAddress.setText("");
        lblImage.setIcon(null);
        txtImagePath.setText("");
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        rbtTemp = new javax.swing.JRadioButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtEmpID = new javax.swing.JTextField();
        txtEmpName = new javax.swing.JTextField();
        txtEmpAge = new javax.swing.JTextField();
        txtContactNo = new javax.swing.JTextField();
        rbtMale = new javax.swing.JRadioButton();
        rbtFemale = new javax.swing.JRadioButton();
        cmbQualification = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        taAddress = new javax.swing.JTextArea();
        btnUploadImage = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        txtImagePath = new javax.swing.JTextField();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        lblImage = new javax.swing.JLabel();
        jdcDOJ = new com.toedter.calendar.JDateChooser();
        cmbBloodGroup = new javax.swing.JComboBox<>();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        btnNew = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        jtbData = new javax.swing.JTable();

        buttonGroup1.add(rbtTemp);
        rbtTemp.setText("blank");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Employee Management");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        jLabel1.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel1.setText("Employee ID");

        jLabel2.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel2.setText("Name");

        jLabel3.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel3.setText("Gender");

        jLabel4.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel4.setText("Age");

        jLabel5.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel5.setText("Blood Group");

        jLabel6.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel6.setText("Contact No");

        jLabel7.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel7.setText("Qualification");

        jLabel8.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel8.setText("DOJ");

        txtEmpID.setEditable(false);
        txtEmpID.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        txtEmpID.setNextFocusableComponent(txtEmpName);

        txtEmpName.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        txtEmpName.setEnabled(false);
        txtEmpName.setNextFocusableComponent(rbtMale);

        txtEmpAge.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        txtEmpAge.setEnabled(false);
        txtEmpAge.setNextFocusableComponent(cmbBloodGroup);

        txtContactNo.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        txtContactNo.setEnabled(false);
        txtContactNo.setNextFocusableComponent(cmbQualification);

        rbtMale.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rbtMale);
        rbtMale.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        rbtMale.setText("Male");
        rbtMale.setEnabled(false);
        rbtMale.setNextFocusableComponent(txtEmpAge);
        rbtMale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtMaleActionPerformed(evt);
            }
        });

        rbtFemale.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rbtFemale);
        rbtFemale.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        rbtFemale.setText("Female");
        rbtFemale.setEnabled(false);
        rbtFemale.setNextFocusableComponent(txtEmpAge);
        rbtFemale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtFemaleActionPerformed(evt);
            }
        });

        cmbQualification.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        cmbQualification.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "BCom.", "BTech.", "MTech.", "CA", "CS", "Other" }));
        cmbQualification.setEnabled(false);
        cmbQualification.setNextFocusableComponent(jdcDOJ);

        jLabel9.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel9.setText("Address");

        taAddress.setColumns(16);
        taAddress.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        taAddress.setRows(5);
        taAddress.setEnabled(false);
        jScrollPane1.setViewportView(taAddress);

        btnUploadImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/image upload.png"))); // NOI18N
        btnUploadImage.setText("Upload Image");
        btnUploadImage.setEnabled(false);
        btnUploadImage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUploadImageActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel10.setText("Image Path");

        txtImagePath.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        txtImagePath.setEnabled(false);

        jDesktopPane1.setBackground(new java.awt.Color(102, 102, 102));

        lblImage.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));

        jDesktopPane1.setLayer(lblImage, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblImage, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                .addContainerGap())
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblImage, javax.swing.GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE)
                .addContainerGap())
        );

        jdcDOJ.setBackground(new java.awt.Color(255, 255, 255));
        jdcDOJ.setEnabled(false);
        jdcDOJ.setNextFocusableComponent(taAddress);

        cmbBloodGroup.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "A+", "A-", "B+", "B-", "AB+", "AB-", "O+", "O-" }));
        cmbBloodGroup.setEnabled(false);
        cmbBloodGroup.setNextFocusableComponent(txtContactNo);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel5)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8))))
                .addGap(20, 20, 20)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(rbtMale)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rbtFemale))
                    .addComponent(txtEmpName)
                    .addComponent(txtEmpID)
                    .addComponent(txtEmpAge)
                    .addComponent(txtContactNo)
                    .addComponent(cmbQualification, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jdcDOJ, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cmbBloodGroup, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(btnUploadImage, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(20, 20, 20)
                        .addComponent(txtImagePath, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)))
                .addGap(20, 20, 20)
                .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtEmpID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel9))
                                .addGap(20, 20, 20)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtEmpName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2))
                                .addGap(20, 20, 20)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel3)
                                    .addComponent(rbtMale)
                                    .addComponent(rbtFemale))
                                .addGap(20, 20, 20)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtEmpAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel4)))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(20, 20, 20)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(btnUploadImage, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbBloodGroup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(20, 20, 20)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtContactNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6)
                            .addComponent(txtImagePath, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10)))
                    .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(cmbQualification, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(jdcDOJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35))
        );

        jPanel2.add(jPanel3);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btnNew.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        btnNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/new-icon.png"))); // NOI18N
        btnNew.setText("New");
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Save-icon.png"))); // NOI18N
        btnSave.setText("Save");
        btnSave.setEnabled(false);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        btnUpdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/update.png"))); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.setEnabled(false);
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/delete.png"))); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.setEnabled(false);
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        btnClear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/clear.png"))); // NOI18N
        btnClear.setText("Clear");
        btnClear.setEnabled(false);
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N
        jLabel12.setText("SEARCH");

        txtSearch.setFont(new java.awt.Font("Roboto", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtSearch, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(btnNew)
                .addGap(18, 18, 18)
                .addComponent(btnSave)
                .addGap(18, 18, 18)
                .addComponent(btnUpdate)
                .addGap(18, 18, 18)
                .addComponent(btnDelete)
                .addGap(18, 18, 18)
                .addComponent(btnClear)
                .addGap(20, 20, 20))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNew)
                    .addComponent(btnSave)
                    .addComponent(btnUpdate)
                    .addComponent(btnDelete)
                    .addComponent(btnClear)
                    .addComponent(jLabel12)
                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5))
        );

        jPanel4.add(jPanel5);

        jtbData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jtbData.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtbDataMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(jtbData);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 976, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane3))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 48, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnUploadImageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUploadImageActionPerformed
        // TODO add your handling code here:
        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(new FileNameExtensionFilter("image", "jpg"));
        chooser.showOpenDialog(this);
        File sourceFile = chooser.getSelectedFile();
        if(sourceFile != null){
            String sourceFilePath = sourceFile.getAbsolutePath();
            txtImagePath.setText(sourceFilePath);
            BufferedImage thumbnail;
            try {
                thumbnail = Thumbnails.of(sourceFile).size(195, 230).asBufferedImage();
                /*
                                ByteArrayOuputStream and ByteArrayInputStream (bascially bridges)
                
                This both stream will help you to convert the thumbnail(Image) to byteArray as humko blob datatype mai store karne ka h
                Agar mereko kispe lihkna h toh OutputStream use karunga aur agar mereko agar kisse lena h toh InputStream use karunga
                matlab isko hum thumbnail dege or else image and then hum ImageIO ka write method use karege joh 
                (thumbnail, "String : to be encoded along with ", OutputStream ka object) and joh thumbnail diya h usko
                uss byteArrayOuputStream mai daalke dega (matlab jab baos banaya tab voh sirf empty object bana raha h,
                ImageIO usko convert karke degas)
                and then joh byte[] ka object h usse iss object ko toByteArray() call kar do
                Toh primitive datatype wala byte array bann jayega!
                
                */
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(thumbnail, "jpg", baos);
              
                
                byteImage = baos.toByteArray();
                
                lblImage.setIcon(new ImageIcon(thumbnail));
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Image Error!");
            }
        }
        
    }//GEN-LAST:event_btnUploadImageActionPerformed

    private void rbtMaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtMaleActionPerformed
        // TODO add your handling code here:
        gender = "Male";
    }//GEN-LAST:event_rbtMaleActionPerformed

    private void rbtFemaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtFemaleActionPerformed
        // TODO add your handling code here:
        gender = "Female";
    }//GEN-LAST:event_rbtFemaleActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
        if(operation == INSERT){
            insertRecord();
        }else if(operation == UPDATE){
            updateRecord();
        }
        operation = NOP;
        btnClear.setEnabled(false);
        btnDelete.setEnabled(false);
        btnNew.setEnabled(true);
        btnSave.setEnabled(false);
        btnUpdate.setEnabled(false);
    }//GEN-LAST:event_btnSaveActionPerformed

    private void jtbDataMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtbDataMouseClicked
        // TODO add your handling code here:
        int selectedRow = jtbData.getSelectedRow();
        String selectedEmpID = jtbData.getModel().getValueAt(selectedRow, 0).toString();
        try{
            String sql = "SELECT * FROM employeeinfo WHERE employee_id = " + selectedEmpID;
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            if(rs.next()){
                txtEmpID.setText(rs.getString("employee_id"));
                txtEmpName.setText(rs.getString("name"));
                
                String genderSelected = rs.getString("gender");
                if("Male".equals(genderSelected)){
                    gender = "Male";
                    rbtMale.setSelected(true);
                }else if("Female".equals(genderSelected)){
                    gender = "Female";
                    rbtFemale.setSelected(true);
                }
                
                txtEmpAge.setText(rs.getString("age"));
                cmbBloodGroup.setSelectedItem(rs.getString("blood_group"));
                txtContactNo.setText(rs.getString("contact_no"));
                cmbQualification.setSelectedItem(rs.getString("qualification"));
                
                String dateValue = rs.getString("doj");
                Date date = new SimpleDateFormat("yyyy-MM-dd").parse(dateValue);
                jdcDOJ.setDate(date);
                
                taAddress.setText(rs.getString("address"));
                
                byteImage = rs.getBytes("employee_image");
                BufferedImage image = ImageIO.read(new ByteArrayInputStream(byteImage));
                ImageIcon icon = new ImageIcon(image);
                lblImage.setIcon(icon);
                disableAll();
                btnUpdate.setEnabled(true);
                btnDelete.setEnabled(true);
                btnNew.setEnabled(true);
                btnSave.setEnabled(false);
                btnClear.setEnabled(false);
                
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "Error : " + e);
        } catch (ParseException ex) {
             JOptionPane.showMessageDialog(this, "Error : " + ex);
        }catch(IOException e){
            JOptionPane.showMessageDialog(this, "Error : " + e);
        }
        
        
    }//GEN-LAST:event_jtbDataMouseClicked

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        // TODO add your handling code here:
        clearFields();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        // TODO add your handling code here:
        int ans = JOptionPane.showConfirmDialog(this, "Are you sure you want to delete this record?", "Delete!", JOptionPane.YES_NO_OPTION);
        if(ans == JOptionPane.YES_OPTION){
            //You have pressed yes, so deleting the selected record!
            String sql = "DELETE FROM employeeinfo WHERE employee_id = ?";
            try {
                ps = conn.prepareStatement(sql);
                ps.setInt(1, Integer.parseInt(txtEmpID.getText()));
                ps.execute();
                sql = "SELECT * FROM employeeinfo";
                updateTableData(sql);
                clearFields();
                btnNew.setEnabled(true);
                btnDelete.setEnabled(false);
                btnClear.setEnabled(false);
                btnUpdate.setEnabled(false);
            }catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Error While Deleting this record!");
            }
        }else if(ans == JOptionPane.NO_OPTION){
            //You have pressed no, so NOP is performed
        }
        
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        // TODO add your handling code here:
        operation = UPDATE;
        enableAll();
        btnNew.setEnabled(false);
        btnUpdate.setEnabled(false);
        btnSave.setEnabled(true);
        btnClear.setEnabled(false);
        btnDelete.setEnabled(false);
       
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        // TODO add your handling code here:
        clearFields();
        enableAll();
        btnNew.setEnabled(false);
        btnSave.setEnabled(true);
        btnClear.setEnabled(true);
        btnDelete.setEnabled(false);
        btnUpdate.setEnabled(false);
        operation = INSERT;
    }//GEN-LAST:event_btnNewActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EmployeeInfo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EmployeeInfo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EmployeeInfo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EmployeeInfo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new EmployeeInfo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JButton btnUploadImage;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cmbBloodGroup;
    private javax.swing.JComboBox<String> cmbQualification;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private com.toedter.calendar.JDateChooser jdcDOJ;
    private javax.swing.JTable jtbData;
    private javax.swing.JLabel lblImage;
    private javax.swing.JRadioButton rbtFemale;
    private javax.swing.JRadioButton rbtMale;
    private javax.swing.JRadioButton rbtTemp;
    private javax.swing.JTextArea taAddress;
    private javax.swing.JTextField txtContactNo;
    private javax.swing.JTextField txtEmpAge;
    private javax.swing.JTextField txtEmpID;
    private javax.swing.JTextField txtEmpName;
    private javax.swing.JTextField txtImagePath;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables
    private String gender = "";
    private byte[] byteImage;
    private int operation;
}
